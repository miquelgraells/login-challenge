# Quasar App (login_page)

A Quasar Framework app

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```


### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).

### Change API url enviroment
Go to file quasar.conf.js and look for line:
API: JSON.stringify('https://prod.api.com')

### Settings or design / architectural decisions
Project structure it's been left as Quasar CLI creates it by default when init project.

At router/index.js every time a location is changed is checked if the user has a token and if the page it is trying to access requires to be logged in, if the user does not has a token then is redirected to login page.

There are 3 pages, Main which is the default page and only it's shown if the user has logged in successfully, therefore ha a token.
Login which is shown if the user does not has a token saved in the VUEX Store.
Error404, default page done by quasar showed when URL not expected.

The login page holds 2 components, LoginForm.vue and ResetPassword.vue.
For such a small page may not be necessary components, but in this case, I thought it would be a good practice to keep functionalities in different components. I thought about doing 3 components, one more for showing the message when the user successfully resets the password, but finally I though It was not worth a whole component for just showing a message and this can be also seen as a functionality of ResetPassword.
Another approach could have been to make a router for the LoginForm and another for the ResetPassword as if they were pages and not components, but that would have been excessive for this simple app.

Mixin used at file /src/boot/mixinInit.js to share a function between the two components to make basic email validation. 

VUEX it's been kept in a modular way even it is not necessary for such a simple web.
In the store has been created the module 'auth' in which it is saved the following states
token: contains the token received from the login API call, used for validating if the user can access or not the main page.
loginState: this can be 'login' or 'resetPassword' used to know which component to show.

Screen designs have been placed at src/statics
Graphic assets have been placet at src/assets

### Things to improve
-Use i18n for multi-language.
-Use ESLint for linter.
