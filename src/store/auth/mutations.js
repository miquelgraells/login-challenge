export function setLoginState (state, loginState) {
    state.loginState = loginState;
}

export function setToken (state, token) {
    state.token = token;
}