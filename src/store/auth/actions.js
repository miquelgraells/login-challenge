export function setLoginSate ({commit}, loginState) {
    commit('setLoginState', loginState);
}

export async function setToken ({commit}, token) {
    commit('setToken', token);
}